import React from 'react';
import './../App.css'
import './All.css'
// import logo from './logo.svg';
// import NavComponent from './Components/NavComponent';

function About() {
  return (
    <div>
      {/* Card */}
      <hr className="text-navy"/>
      <div className="container text-center">
          <div className="col-md">
            <div className="card text-white bg-navy mb-3">
                <div className="card-header">About Me</div>
                <div className="card-body bg-light">
                  <h5 className="card-title text-navy">Muhammad Adnand Jody Pratama</h5>
                  <img className="img-circle" width="300px" src="https://drive.google.com/uc?export=view&id=1ghxOFmdOy7ex6wq9VAO3LfAUsd3XmJ1v" alt=""/>
                  <p className="card-text text-navy">Jody is a fresh graduate of Computer Engineering from Politeknik Negeri Sriwijaya, Palembang. After graduate, his eagerness to learn more about Web Programming leads him to join Glints Academy Bootcamp. Now he is currently on Batch 9 of this Bootcamp and still learning about Front-End Web Programming using React JS as the front-end library.</p>
                  <hr className="text-navy"/>
                  {/* Accordion */}
                  <div className="accordion accordion-flush text-center text-navy" id="accordionExample">
                    <div className="accordion-item">
                      <h2 className="accordion-header" id="headingOne">
                        <button className="accordion-button" type="button" data-bs-toggle="collapse" data-bs-target="#collapseOne" aria-expanded="true" aria-controls="collapseOne">
                          <p className="text-navy text-center"><strong>Hobbies</strong></p>
                        </button>
                      </h2>
                      <div id="collapseOne" className="accordion-collapse collapse" aria-labelledby="headingOne" data-bs-parent="#accordionExample">
                        <div className="accordion-body">
                          <p>He loves to sing some songs just to make himself happy. Sometimes he cooks even when he is not hungry at that time.</p>
                        </div>
                      </div>
                    </div>

                    <div className="accordion-item">
                      <h2 className="accordion-header" id="headingOne">
                        <button className="accordion-button" type="button" data-bs-toggle="collapse" data-bs-target="#collapseOne" aria-expanded="true" aria-controls="collapseOne">
                          <p className="text-navy text-center"><strong>Hopes</strong></p>
                        </button>
                      </h2>
                      <div id="collapseOne" className="accordion-collapse collapse" aria-labelledby="headingOne" data-bs-parent="#accordionExample">
                        <div className="accordion-body">
                          <p>Besides becoming a reliable web programmer, Jody is hoping he can make everyone in this world happy.</p>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
                <div class="card-body text-light bg-navy">
                  <h5>CONTACT ME</h5>
                  <img src="https://cdn.icon-icons.com/icons2/644/PNG/512/red_phone_icon-icons.com_59526.png" width="20px" alt=""/><span> +628 51 5502 2172</span>
                  <br/>
                  <br/>
                  <p>or kindly send me email :</p>
                  <p>MUHAMMAD.ADNAND.JODY@GMAIL.COM</p>
                </div>
              </div>
          </div>
      </div>
      <hr className="text-navy"/>
    </div>
  );
}

export default About;

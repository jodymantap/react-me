import React from 'react';
import './All.css'
// import logo from './logo.svg';
// import NavComponent from './Components/NavComponent';
import { Link } from 'react-router-dom';

function NavComponent() {
  return (
    <div>
      {/* Navbar */}
      <div>
        <nav className="navbar navbar-expand-lg navbar-dark bg-navy">
          <div className="container-fluid">
            <Link to="/">
            <a className="navbar-brand nav-brand">MyApp</a>
            </Link>
            <button className="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#navbarNav" aria-controls="navbarNav" aria-expanded="false" aria-label="Toggle navigation">
              <span className="navbar-toggler-icon"></span>
            </button>
            <div className="collapse navbar-collapse" id="navbarNav">
              <ul className="navbar-nav">
                <li className="nav-item">
                  <Link to="/">
                  <a className="nav-link">Home</a>
                  </Link>
                </li>
                <li className="nav-item">
                  <Link to="/activities">
                  <a className="nav-link">Activities</a>
                  </Link>
                </li>
                <li className="nav-item">
                  <Link to="/about">
                  <a className="nav-link">About</a>
                  </Link>
                </li>
                <li className="nav-item">
                  <Link to="/contact">
                  <a className="nav-link">Contact Me</a>
                  </Link>
                </li>
              </ul>
            </div>
          </div>
        </nav>
      </div>
    </div>
  );
}

export default NavComponent;

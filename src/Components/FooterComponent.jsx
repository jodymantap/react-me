import React from 'react';
import './../App.css'
import './All.css'
// import logo from './logo.svg';
// import NavComponent from './Components/NavComponent';

function FooterComponent() {
  return (
    <div>
      <div className="footer">
        <h4>- Made with &#9829; by Jody Mantap -</h4>
      </div>
      <br/>
    </div>
  );
}

export default FooterComponent;

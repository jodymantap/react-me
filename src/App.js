import React from 'react';
// import logo from './logo.svg';
import NavComponent from './Components/NavComponent';
import FooterComponent from './Components/FooterComponent';
import {Switch, Route, BrowserRouter as Router} from 'react-router-dom';
import About from './Components/About';
import Activities from './Components/Activities';
import Home from './Components/Home';
import Contact from './Components/Contact';

function App() {
  return (
    <Router>
    <div className="App">
      <NavComponent/>
        <Switch>
          <Route exact path="/" component={Home}/>
          <Route exact path="/activities" component={Activities}/>
          <Route exact path="/about" component={About}/>
          <Route exact path="/contact" component={Contact}/>
        </Switch>
      <FooterComponent/>
    </div>
    </Router>
  );
}

export default App;
